<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Translation;

use Symfony\Component\Intl\Exception\RuntimeException;

/**
 * Class TranslationLocaleProvider.
 *
 * @author Jérôme Fath
 */
final class TranslationLocaleProvider implements TranslationLocaleProviderInterface
{
    /** @var string */
    private $defaultLocaleCode;

    /** @var array */
    private $definedLocalesCodes;

    /**
     * {@inheritdoc}
     */
    public function getDefaultLocaleCode(): string
    {
        return $this->defaultLocaleCode;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefinedLocalesCodes(): array
    {
        return $this->definedLocalesCodes;
    }

    /**
     * {@inheritdoc}
     */
    public function setLocalesCodes(string $defaultLocaleCode, array $definedLocalesCodes): TranslationLocaleProviderInterface
    {
        $this->definedLocalesCodes = $definedLocalesCodes;
        $this->defaultLocaleCode = $this->retrieveDefinedLocaleCode($defaultLocaleCode);

        return $this;
    }

    /**
     * @param string $defaultLocaleCode
     *
     * @return string
     */
    private function retrieveDefinedLocaleCode(string $defaultLocaleCode)
    {
        $locale = explode('_', $defaultLocaleCode)[0];
        foreach ($this->definedLocalesCodes as $definedLocaleCode) {
            if ($locale === explode('_', $definedLocaleCode)[0]) {
                return $definedLocaleCode;
            }
        }

        throw new RuntimeException(sprintf('Locale "%s" is not available in bundle config', $locale));
    }
}
