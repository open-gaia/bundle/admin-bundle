<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class ImagineExtension.
 *
 * @author Jérôme Fath
 */
final class ImagineExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('imagine_filter', [$this, 'filter']),
        ];
    }

    /**
     * @param $path
     * @param $filter
     * @param array $config
     * @param null  $resolver
     */
    public function filter($path, $filter, array $config = [], $resolver = null)
    {
        return;
    }
}
