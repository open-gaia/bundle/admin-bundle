<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Twig;

use Gaia\Bundle\AdminBundle\Locale\LocaleConverterInterface;
use Locale;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class LocaleExtension.
 *
 * @author Jérôme Fath
 */
final class LocaleExtension extends AbstractExtension
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var LocaleConverterInterface
     */
    private $localeConverter;

    /**
     * LocaleExtension constructor.
     *
     * @param RequestStack             $requestStack
     * @param LocaleConverterInterface $localeConverter
     */
    public function __construct(RequestStack $requestStack, LocaleConverterInterface $localeConverter)
    {
        $this->requestStack = $requestStack;
        $this->localeConverter = $localeConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('sylius_locale_name', [$this, 'convertLocaleCodeToName']),
            new TwigFilter('sylius_locale_country', [$this, 'convertCountryCode']),
        ];
    }

    /**
     * @param string $code
     *
     * @return string
     */
    public function convertLocaleCodeToName(string $code): string
    {
        return $this->localeConverter->convertCodeToName(
            $code, $this->requestStack->getCurrentRequest()->getLocale()
        );
    }

    /**
     * @param string $locale
     *
     * @return string|null
     */
    public function convertCountryCode(string $locale): ?string
    {
        return empty(Locale::getRegion($locale))
            ? Locale::getPrimaryLanguage($locale)
            : Locale::getRegion($locale)
        ;
    }
}
