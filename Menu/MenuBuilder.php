<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Menu;

use Gaia\Bundle\AdminBundle\GaiaAdminEvents;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class MenuBuilder.
 *
 * @author Jérôme Fath
 */
final class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * MenuBuilder constructor.
     *
     * @param FactoryInterface         $factory
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(FactoryInterface $factory, EventDispatcherInterface $eventDispatcher)
    {
        $this->factory = $factory;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createMenuSidebar(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root')->setAttribute('position', 'right');

        $this->eventDispatcher->dispatch(new MenuBuilderEvent($this->factory, $menu), GaiaAdminEvents::MENU_SIDEBAR);

        return $menu;
    }

    /**
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createMenuTopbar(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        $this->eventDispatcher->dispatch(new MenuBuilderEvent($this->factory, $menu), GaiaAdminEvents::MENU_TOPBAR);

        return $menu;
    }
}
