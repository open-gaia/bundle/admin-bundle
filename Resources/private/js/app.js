import 'semantic-ui-css/components/api';
import 'semantic-ui-css/components/accordion';
import 'semantic-ui-css/components/dropdown';
import 'semantic-ui-css/components/transition';

import $ from 'jquery';

import './sylius-compound-form-errors';

$(document).ready(() => {
    $(document).ready(function () {
        $('table thead th.sortable').on('click', function () {
            window.location = $(this).find('a').attr('href');
        });

        $('select.dropdown.semantic').dropdown();

        $('.sylius-autocomplete').autoComplete();
        $('.sylius-autocomplete').dropdown('setting', 'clearable', true);

        $('.sylius-tabular-form').addTabErrors();
        $('.ui.accordion').addAccordionErrors();
    });
});
