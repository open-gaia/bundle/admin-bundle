<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle;

use Gaia\Bundle\AdminBundle\DependencyInjection\Compiler\GridExporterCompilerPass;
use Gaia\Bundle\AdminBundle\DependencyInjection\Compiler\LocaleRequestCompilerPass;
use Gaia\Bundle\AdminBundle\DependencyInjection\Compiler\ResourceControllerEventListenerCompilerPass;
use Gaia\Bundle\AdminBundle\DependencyInjection\Compiler\TranslationLocaleProviderCompilerPass;
use Gaia\Bundle\AdminBundle\Exporter\GridExporterInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class GaiaBundleAdminBundle.
 *
 * @author Jérôme Fath
 */
final class GaiaAdminBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container
            ->registerForAutoconfiguration(GridExporterInterface::class)
            ->addTag('gaia_admin.grid_exporter')
        ;

        $container
            ->addCompilerPass(new GridExporterCompilerPass())
            ->addCompilerPass(new LocaleRequestCompilerPass())
            ->addCompilerPass(new TranslationLocaleProviderCompilerPass())
            ->addCompilerPass(new ResourceControllerEventListenerCompilerPass())
        ;
    }
}
