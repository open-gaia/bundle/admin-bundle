<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Security\ExpressionLanguage;

use Sylius\Bundle\ResourceBundle\Controller\AuthorizationCheckerInterface;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Bundle\ResourceBundle\Controller\SingleResourceProviderInterface;
use Sylius\Component\Registry\ServiceRegistryInterface;
use Sylius\Component\Resource\ResourceActions;

final class AuthorizationChecker implements AuthorizationCheckerInterface
{
    public function __construct(
        private AuthorizationCheckerExpression $authorizationCheckerExpression,
        private ServiceRegistryInterface $resourceRepositoryRegistry,
        private SingleResourceProviderInterface $singleResourceProvider,
    ){}

    public function isGranted(RequestConfiguration $configuration, string $expression): bool
    {
        if(!$this->hasResource($configuration)) {
            return $this->authorizationCheckerExpression->evaluate($expression);
        }

        $metadata = $configuration->getMetadata();
        $resourceName = sprintf('%s.%s', $metadata->getApplicationName(), $metadata->getName());
        $repository = $this->resourceRepositoryRegistry->get($resourceName);
        $resource = $this->singleResourceProvider->get($configuration, $repository);

        return $this->authorizationCheckerExpression->evaluate($expression, $resource);
    }

    private function hasResource(RequestConfiguration $configuration): bool
    {
        $controller = $configuration->getRequest()->get('_controller');

        $actions =  [
            ResourceActions::SHOW,
            ResourceActions::CREATE,
            ResourceActions::UPDATE,
            ResourceActions::DELETE
        ];

        foreach ($actions as $action) {
            if(false !== strpos($controller, sprintf(':%s', $action))) {
                return true;
            }
        }
        return false;
    }
}
