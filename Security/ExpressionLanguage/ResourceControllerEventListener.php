<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Security\ExpressionLanguage;

use Sylius\Bundle\ResourceBundle\Controller\RequestConfigurationFactoryInterface;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Resource\Model\ResourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final class ResourceControllerEventListener
{
    public function __construct(
        private RequestConfigurationFactoryInterface $requestConfigurationFactory,
        private AuthorizationCheckerExpression $authorizationCheckerExpression,
        private RequestStack $requestStack,
        private array $configuration
    ){}

    public function on(ResourceControllerEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();
        if(null === $request) {
            return;
        }

        $controller = $request->attributes->get('_controller');
        if(isset($this->configuration[$controller])) {
            $expression = $this->configuration[$controller];
        }
        else {
            $controller = str_replace('::', ':', $controller);
            $expression = $this->configuration[$controller] ?? null;
        }

        if(null === $expression) {
            return;
        }

        $resource = $event->getSubject() instanceof ResourceInterface ? $event->getSubject() : null;
        if (!$this->authorizationCheckerExpression->evaluate($expression, $resource)) {
            throw new AccessDeniedException();
        }
    }
}
