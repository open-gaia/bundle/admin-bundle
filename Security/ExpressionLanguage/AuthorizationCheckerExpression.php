<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Security\ExpressionLanguage;

use Sylius\Bundle\ResourceBundle\Controller\ParametersParserInterface;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Bundle\ResourceBundle\Controller\SingleResourceProviderInterface;
use Sylius\Component\Registry\ServiceRegistryInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class AuthorizationCheckerExpression
{
    public function __construct(
        private ContainerInterface $container,
        private ExpressionLanguage $expressionLanguage
    ){}

    public function evaluate(string $expression, ?ResourceInterface $resource = null)
    {
        /** @var Request $request */
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $token = $this->container->get('security.token_storage')->getToken();

        return $this->expressionLanguage->evaluate($expression, [
            'container' => $this->container,
            'auth_checker' => $this->container->get('security.authorization_checker'),
            'request' => $request,
            'params' => $request->attributes->get('_route_params'),
            'token' => $token,
            'user' =>  $token?->getUser(),
            'resource' => $resource
        ]);
    }
}
