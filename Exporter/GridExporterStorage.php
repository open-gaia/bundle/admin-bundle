<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Exporter;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Component\Grid\View\GridViewInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GridExporterStorage.
 *
 * @author Jérôme Fath
 */
final class GridExporterStorage implements GridExporterStorageInterface
{
    /**
     * @var ArrayCollection
     */
    private $collection;

    /**
     * {@inheritdoc}
     */
    public function add(GridExporterInterface $gridHandler): void
    {
        if ($this === $gridHandler) {
            return;
        }

        $this->collection->add($gridHandler);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(GridExporterInterface $gridHandler): void
    {
        $this->collection->remove($gridHandler);
    }

    /**
     * GridHandlerStorage constructor.
     */
    public function __construct()
    {
        $this->collection = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'type' => 'chainer',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(RequestConfiguration $requestConfiguration, GridViewInterface $gridView, array $options = []): Response
    {
        $resolver = $this->createOptionsResolver();

        /** @var GridExporterInterface $gridHandler */
        foreach ($this->collection as $gridHandler) {
            $gridHandler->configureOptions($resolver);

            $reference = $resolver->resolve()['name'];
            $options = $resolver->resolve(
                $requestConfiguration->getRequest()->query->get('exporter', [])
            );

            if ($reference === $options['name']) {
                return $gridHandler->handle($requestConfiguration, $gridView, $options);
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * @return OptionsResolver
     */
    private function createOptionsResolver(): OptionsResolver
    {
        $resolver = new OptionsResolver();

        $resolver
            ->setRequired('name')
            ->setAllowedTypes('name', 'string')
        ;

        return $resolver;
    }
}
