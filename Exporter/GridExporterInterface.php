<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Exporter;

use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Component\Grid\View\GridViewInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Interface GridExporterInterface.
 *
 * @author Jérôme Fath
 */
interface GridExporterInterface
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void;

    /**
     * @param RequestConfiguration $requestConfiguration
     * @param GridViewInterface    $gridView
     * @param array                $options
     *
     * @return Response
     */
    public function handle(RequestConfiguration $requestConfiguration, GridViewInterface $gridView, array $options = []): Response;
}
