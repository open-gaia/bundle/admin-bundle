<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Exporter\Extractor;

use Sylius\Component\Grid\Definition\Grid;

/**
 * Interface GridExtractorInterface.
 *
 * @author Jérôme Fath
 */
interface GridExtractorInterface
{
    /**
     * @param Grid $definition
     *
     * @return mixed
     */
    public function extract(Grid $definition);
}
