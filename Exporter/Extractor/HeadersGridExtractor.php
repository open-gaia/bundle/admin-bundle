<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Exporter\Extractor;

use Sylius\Component\Grid\Definition\Field;
use Sylius\Component\Grid\Definition\Grid;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class HeadersGridExtractor.
 *
 * @author Jérôme Fath
 */
final class HeadersGridExtractor implements GridExtractorInterface
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * HeadersGridExtractor constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function extract(Grid $definition)
    {
        $headers = [];

        /** @var Field $field */
        foreach ($definition->getFields() as $field) {
            $headers[] = $this->translator->trans($field->getLabel());
        }

        return $headers;
    }
}
