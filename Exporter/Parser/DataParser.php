<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Exporter\Parser;

use Closure;
use Pagerfanta\Pagerfanta;
use Sylius\Component\Grid\Definition\Field;
use Sylius\Component\Grid\Definition\Grid;
use Sylius\Component\Grid\FieldTypes\FieldTypeInterface;
use Sylius\Component\Grid\View\GridViewInterface;
use Sylius\Component\Registry\ServiceRegistryInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataParser.
 *
 * @author Jérôme Fath
 */
final class DataParser implements DataParserInterface
{
    /**
     * @var ServiceRegistryInterface
     */
    private $fieldsRegistry;

    /**
     * DataParser constructor.
     *
     * @param ServiceRegistryInterface $fieldsRegistry
     */
    public function __construct(ServiceRegistryInterface $fieldsRegistry)
    {
        $this->fieldsRegistry = $fieldsRegistry;
    }

    /**
     * {@inheritdoc}
     */
    public function parse(GridViewInterface $gridView, Pagerfanta $paginator, Closure $closure): void
    {
        $this->parseResources($gridView->getDefinition(), $paginator, $closure);

        while ($paginator->hasNextPage()) {
            $this->parseResources($gridView->getDefinition(), $paginator, $closure);

            $paginator->setCurrentPage($paginator->getNextPage());
        }
    }

    /**
     * @param Grid       $definition
     * @param Pagerfanta $paginator
     * @param Closure    $closure
     */
    private function parseResources(Grid $definition, Pagerfanta $paginator, Closure $closure)
    {
        foreach ($paginator as $resource) {
            $closure($this->extractData($definition, $resource));
        }
    }

    /**
     * @param Grid              $definition
     * @param ResourceInterface $resource
     *
     * @return array
     */
    private function extractData(Grid $definition, ResourceInterface $resource): array
    {
        $data = [];
        foreach ($definition->getFields() as $field) {
            $data[] = $this->renderField($field, $resource);
        }

        return $data;
    }

    /**
     * @param Field             $field
     * @param ResourceInterface $resource
     *
     * @return string
     */
    private function renderField(Field $field, ResourceInterface $resource): string
    {
        /** @var FieldTypeInterface $fieldType */
        $fieldType = $this->fieldsRegistry->get($field->getType());
        $resolver = new OptionsResolver();
        $fieldType->configureOptions($resolver);
        $options = $resolver->resolve($field->getOptions());

        return $fieldType->render($field, $resource, $options);
    }
}
