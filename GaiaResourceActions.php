<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle;

/**
 * Class GaiaResourceActions.
 *
 * @author Jérôme Fath
 */
final class GaiaResourceActions
{
    public const EXPORT = 'export';

    private function __construct()
    {
    }
}
