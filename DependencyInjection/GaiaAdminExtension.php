<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\DependencyInjection;

use Gaia\Bundle\AdminBundle\Controller\ResourceController;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class GaiaAdminExtension.
 *
 * @author Jérôme Fath
 */
final class GaiaAdminExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $this->processConfiguration($this->getConfiguration([], $container), $config);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        $config = $this->processConfiguration(new Configuration(), $container->getExtensionConfig($this->getAlias()));

        $twigConfig = [];
        $twigConfig['globals']['gaia_admin_dashboard_route'] = $config['dashboard']['route'];
        $container->prependExtensionConfig('twig', $twigConfig);

        $resourceConfig = [];
        $resourceConfig['resources'] = $this->retrieveResourcesConfig($container);
        $resourceConfig['authorization_checker'] = $config['security']['authorization_checker'];
        $resourceConfig['translation']['enabled'] = $config['translation']['enabled'];
        $resourceConfig['translation']['locale_provider'] = $config['translation']['locale_provider'];
        $container->prependExtensionConfig('sylius_resource', $resourceConfig);
    }

    /**
     * @param ContainerBuilder $container
     *
     * @return array
     */
    private function retrieveResourcesConfig(ContainerBuilder $container): array
    {
        $config = $container->getExtensionConfig('sylius_resource')[0];
        if (empty($config['resources'])) {
            return [];
        }

        $resources = [];
        foreach ($config['resources'] as $keyResource => $configResource) {
            if (empty($configResource['classes']['controller'])) {
                $configResource['classes']['controller'] = ResourceController::class;
            }
            $resources[$keyResource] = $configResource;
        }

        return $resources;
    }
}
