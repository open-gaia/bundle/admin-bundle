<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\DependencyInjection\Compiler;

use Gaia\Bundle\AdminBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class LocaleRequestCompilerPassimplements
 *
 * @author Jérôme Fath
 */
final class LocaleRequestCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('gaia_admin.locale_request')) {
            return;
        }

        $config = (new Processor())->processConfiguration(
            new Configuration(), $container->getExtensionConfig('gaia_admin')
        );

        $definition = $container->getDefinition('gaia_admin.locale_request');
        $definition->setArgument('$defaultLocales', $config['translation']['locales']);

        foreach ($config['request']['locales'] as $requestLocale => $translationLocale) {
            $definition->addMethodCall('add', [
                $requestLocale, $translationLocale
            ]);
        }
    }
}