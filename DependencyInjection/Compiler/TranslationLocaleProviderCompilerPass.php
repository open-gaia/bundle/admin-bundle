<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\DependencyInjection\Compiler;

use Gaia\Bundle\AdminBundle\DependencyInjection\Configuration;
use Gaia\Bundle\AdminBundle\Translation\TranslationLocaleProviderInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * Class TranslationLocaleProviderCompilerPass.
 *
 * @author Jérôme Fath
 */
final class TranslationLocaleProviderCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $processor = new Processor();
        $config = $processor->processConfiguration(
            new Configuration(), $container->getExtensionConfig('gaia_admin')
        );

        $providerServiceId = $config['translation']['locale_provider'];
        $locales = $config['translation']['locales'];

        $definition = $container->getDefinition($providerServiceId);

        if (!in_array(TranslationLocaleProviderInterface::class, class_implements($definition->getClass()))) {
            throw new RuntimeException(sprintf('Your locale provider must be implements %s', TranslationLocaleProviderInterface::class));
        }

        $definition->addMethodCall('setLocalesCodes', [
            $container->getParameter('kernel.default_locale'), $locales,
        ]);
    }
}
