<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\DependencyInjection\Compiler;

use Gaia\Bundle\AdminBundle\DependencyInjection\Configuration;
use Gaia\Bundle\AdminBundle\DependencyInjection\Guesser\ResourceControllerEventGuesser;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ResourceControllerEventListenerCompilerPass.
 *
 * @author Jérôme Fath
 */
final class ResourceControllerEventListenerCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $processor = new Processor();
        $config = $processor->processConfiguration(
            new Configuration(), $container->getExtensionConfig('gaia_admin')
        );

        $definition = $container->getDefinition('gaia_admin.security.expression_language_listener');
        $definition->setArgument('$configuration', $config['security']['expression_language']);

        $resourceControllerEventGuesser = new ResourceControllerEventGuesser();
        foreach ($config['security']['expression_language'] as $key => $value) {
            foreach ($resourceControllerEventGuesser->process($key) as $event) {
                $definition->addTag('kernel.event_listener', [
                    'event' => $event,
                    'method' => 'on',
                ]);
            }
        }
    }
}
