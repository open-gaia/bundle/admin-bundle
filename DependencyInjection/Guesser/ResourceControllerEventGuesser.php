<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\DependencyInjection\Guesser;

use Sylius\Component\Resource\ResourceActions;

final class ResourceControllerEventGuesser
{
    const EVENTS = [
        ResourceActions::INDEX => ['index'],
        ResourceActions::SHOW => ['show'],
        ResourceActions::CREATE => ['pre_create', 'initialize_create'],
        ResourceActions::UPDATE => ['pre_update', 'initialize_update'],
        ResourceActions::DELETE => ['pre_delete'],
        ResourceActions::BULK_DELETE => ['bulk_delete'],
    ];

    public function process(string $value): array
    {
        list($applicationName, $name) = explode('.', str_replace('.controller', '', strstr($value, ':', true)));

        $action = str_replace('Action', '', substr(strrchr($value, ':'), 1));

        if (!isset(self::EVENTS[$action])) {
            return [];
        }

        $events = [];
        foreach (self::EVENTS[$action] as $event) {
            $events[] = sprintf('%s.%s.%s', $applicationName, $name, $event);
        }

        return $events;
    }
}
