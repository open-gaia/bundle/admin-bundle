GaiaAdminBundle
===============

This admin bundle use beautiful [SyliusResourceBundle](https://github.com/Sylius/SyliusResourceBundle) to manage easy CRUD and [Webpack Encore](https://github.com/symfony/webpack-encore-bundle) to manage resources.

Configuration
-------------

```yaml
imports:
  - { resource: "@GaiaAdminBundle/Resources/config/config.yaml" }
```

### Webpack Encore

Add [workspace](https://yarnpkg.com/lang/en/docs/workspaces/) in your project "package.json" :

```json
{
  "workspaces": [
    "./vendor/open-gaia/admin-bundle"
  ]
}
```

Add in your project "webpack.config.js" :

```js
const Encore = require('@symfony/webpack-encore');
const adminConfig = require('./vendor/open-gaia/admin-bundle/webpack.config');

Encore.reset();

// config

module.exports = [Encore.getWebpackConfig(), config];
```

