<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Currency;

use InvalidArgumentException;

/**
 * Interface CurrencyConverterInterface.
 *
 * @author Jérôme Fath
 */
interface CurrencyConverterInterface
{
    /**
     * @param string      $code
     * @param string|null $locale
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function convertCodeToSymbol(string $code, ?string $locale = null): string;
}
