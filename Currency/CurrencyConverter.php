<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Currency;

use Symfony\Component\Intl\Currencies;
use Webmozart\Assert\Assert;

/**
 * Class CurrencyConverter.
 *
 * @author Jérôme Fath
 */
final class CurrencyConverter implements CurrencyConverterInterface
{
    /**
     * {@inheritdoc}
     */
    public function convertCodeToSymbol(string $code, ?string $locale = null): string
    {
        $symbol = Currencies::getSymbol($code, $locale);
        Assert::string($symbol, sprintf('Cannot find symbol for "%s" locale code', $code));

        return $symbol;
    }
}
