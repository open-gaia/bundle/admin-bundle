<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Locale;

use InvalidArgumentException;

/**
 * Interface LocaleConverterInterface.
 *
 * @author Jérôme Fath
 */
interface LocaleConverterInterface
{
    /**
     * @param string      $name
     * @param string|null $locale
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function convertNameToCode(string $name, ?string $locale = null): string;

    /**
     * @param string      $code
     * @param string|null $locale
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function convertCodeToName(string $code, ?string $locale = null): string;
}
