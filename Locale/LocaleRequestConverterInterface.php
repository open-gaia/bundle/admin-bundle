<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Locale;

/**
 * Interface LocaleRequestConverterInterface
 *
 * @author Jérôme Fath
 */
interface LocaleRequestConverterInterface
{
    public function convertCurrentToTranslationLocale(): string;

    public function convertToTranslationLocale(string $locale): string;
}