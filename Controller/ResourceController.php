<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Controller;

use Gaia\Bundle\AdminBundle\Exporter\GridExporterStorageInterface;
use Gaia\Bundle\AdminBundle\GaiaResourceActions;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController as BaseResourceController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResourceController.
 *
 * @author Jérôme Fath
 */
class ResourceController extends BaseResourceController
{
    /**
     * @param Request                      $request
     * @param GridExporterStorageInterface $gridExporterStorage
     *
     * @return Response
     */
    public function exportAction(Request $request, GridExporterStorageInterface $gridExporterStorage): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, GaiaResourceActions::EXPORT);
        $resources = $this->resourcesCollectionProvider->get($configuration, $this->repository);

        $response = $gridExporterStorage->handle($configuration, $resources);

        $this->eventDispatcher->dispatchMultiple(GaiaResourceActions::EXPORT, $configuration, $resources);

        return $response;
    }
}
